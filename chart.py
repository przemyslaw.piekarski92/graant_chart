import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from datetime import datetime

# Defining the project phases and their durations
phases = {
    "Inicjacja Projektu": ("2024-04-01", "2024-05-01"),
    "Planowanie Projektu": ("2024-05-02", "2024-07-01"),
    "Analiza Wymagań": ("2024-07-02", "2024-08-01"),
    "Projektowanie Systemu": ("2024-08-02", "2024-10-01"),
    "Rozwój i Implementacja": ("2024-10-02", "2025-03-01"),
    "Testowanie": ("2025-03-02", "2025-05-01"),
    "Wdrożenie": ("2025-05-02", "2025-09-01"),
    "Zamknięcie i Przekazanie Projektu": ("2025-09-02", "2025-11-01")
}

# Converting dates to datetime objects and adjusting order
for phase, dates in phases.items():
    phases[phase] = [datetime.strptime(date, "%Y-%m-%d") for date in dates]

# Plotting
fig, ax = plt.subplots(figsize=(12, 8))

# Each phase as a separate bar, ordered from bottom to top
for phase, dates in reversed(list(phases.items())):
    start, end = dates
    ax.barh(phase, end - start, left=start, color='skyblue', edgecolor='grey')

# Formatting the date display
locator = mdates.AutoDateLocator(minticks=3, maxticks=7)
formatter = mdates.ConciseDateFormatter(locator)
ax.xaxis.set_major_locator(locator)
ax.xaxis.set_major_formatter(formatter)

ax.set_xlabel("Data")
ax.set_ylabel("Faza projektu")
plt.title("Wykres Gantta dla Projektu IT")
plt.grid(True)

# Show the Gantt chart
plt.tight_layout()
plt.show()
